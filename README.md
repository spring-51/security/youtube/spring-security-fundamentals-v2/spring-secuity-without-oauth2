# Spring Security Fundamentals
Tutor : Laurentiu Spilca <br />
Platform: youtube <br />
Channel: Spring Security Fundamentals <br />

```
here we have kep code changes till lecture 10
```

```
from lecture 23 we resumed this repo
```

### Lecture 23 - Configuring Endpoint( Way1 )

```
1. There are 3 ways to config authorization i.e. once user is authenticated successfully,
then whether it is eligible to perform the operation.
- way 1 - Endpoint(PATH + HTTP VERB) authorization rules
  -- it uses spring security filter chain to do authorization
- way 2 - Global method security
  - i.e. @RoleAllowed,
         @Pre/@Post Authorize,
         @Pre/@Post Filter,
         @Secured
  -- it uses AOP to do authorization

- way 3 - using .

2. here we learned how can we config endpoint(way 1) to be accessible to a user of specific authority or role

3. ROLE VS AUTHORITY
/** ROLE VS AUTHORITY
 *
 * 1. ROLE meaning NOUN(e.g. manager, admin etc)
 *    AUTHORITY - meaning VERB(e.g. create,read,update, delete etc).
 *
 * 2. AUTHORITY mean more granular level permissions.
 * e.g. createUser
 *
 * 3. if we use ROLE we need to tell spring security to use GrantedAuthority
 * as ROLE by prefix "ROLE_"
 * EITHER before saving user to the db
 *   - refer - SpringbootAuthServerApplication -> run(String... args)
 * OR before adding authority to Authentication
 *   - refer - TokenAuthenticationProvider -> authenticate
 *
 * TO BE CONTINUED...
 */
```

### Lecture 24( Way1 )

```
1. ROLE VS AUTHORITY(CONTINUED FROM lesson 23)

*
* 4. ROLE means a group of permission
*    e.g. For an ADMIN - createAdminAuthority, viewAdminAuthority, updateAdminAuthority etc.
*    AUTHORITY means single permission
*    e.g. createAdminAuthority - can only create an admin

2. Url Matchers - there are 3 matchers
 - refer - ProjectConfig -> configure(HttpSecurity http)
 [MOST RECOMMENDED]
 2.1 : mvcMatchers :
 2.2 : antMatchers :
 [LEAST RECOMMENDED]
 2.3 : regexMatcher :


```

### Lecture 25( Way1 )
```
1. mvc matcher VS ant matcher
/**
 * [MOST RECOMMENDED]
 * mvc matcher
 *  - it uses ANT path expression(s)
 *  - spring recommends to use mvc matcher over other matcher
 *
 * ant matcher
 *  - it also uses ANT path expression(s)
 *  - spring recommends to use mvc matcher over ant matcher
 *
 * [LEAST RECOMMENDED]
 * regex matchers
 * - spring recommends to use mvc matcher over regex matcher
 */

2.
/** ANT MATCHER SYNTAX */
// 1. "/a/**" - any path which starts with "/a"  with ZERO or MULTIPLE
//  - eg. of eligible path /a , /a/b, /a/b/c etc.

// 2. "/a/*" -  - any path which starts with "/a" followed by EXACT ONE more /
// - eg. of eligible path  /a/b etc
// - /a , /a/b/c - ARE NOT A VALID PATH FOR THIS ANT MATCHER

// 3. "/a/**/b" - any path which starts with "/a" followed by ZERO or MULTIPLE   and ends with /b
// - eg. of eligible path /a/b , /a/b/b , /a/c/b, /a/b/c/a/b etc

// 4. "/a/*/b" - any path which starts with "/a" followed by EXACT ONE and ends with /b
// - eg. of eligible path  /a/b/b , /a/c/b etc
// - /a/b , /a/b/c/a/b ARE NOT A VALID PATH FOR THIS ANT MATCHER

refer -  ProjectConfig -> configure(HttpSecurity http)
```

### Lecture 27( way 2 - Global method security : it uses AOP)
```
1. When we use this way of authorization, we need to enable this using @EnableGlobalMethodSecurity,
  and enable the config annotation that we need, by default all the config annotation is DISABLED
  - refer ProjectConfig

2. Once its enabled then we have 6 different ways to config authorization
  [MOSTLY USED CONFIGS]
  2.1 - using @PreAuthorization
        /*
            @PreAuthorize(value = "hasRole('ADMIN')")
            the authorization rules are validated against the current Authentication Object
            before calling the below method and decide whether to call the below method or return 403
            without calling the method.

            internally the aspect(AOP) does the above validation and as per validation result
            it either calls the method or reject it with 403
        */
        - refer - MethodLevelAdminService -> getAll()
  2.2 - using @PostAuthorization
        /*
            @PostAuthorize(value = "hasRole('ADMIN')")
            the method is called ALWAYS and only then aspect validates the authorization rules against the current Authentication Object
            and decide whether to return response from the method or return 403.

            internally the aspect(AOP) does the above validation AFTER CALLING the method and method executed
            and as per validation result it either return the response from the method or return 403

            use case - use @PostAuthorize if we want to perform authorization
            rule on returned value (using returnObject) of the method
            - refer MethodLevelAdminService -> testPostAuthorizeUseCase
        */
        - refer - MethodLevelAdminService -> getAll()
  2.3 - using @PreFilter
    /*
        The method needs to have THE PARAMETER value of Collection or Array else IT WILL NOT WORK
        The aspect intercept the method call and validates the value inside the the collection or
        array parameter
     */
  2.4 - using @PostFilter
     /*
         @PostFilter(value = ?)
         The method needs to have THE RETURN value of Collection or Array else IT WILL NOT WORK.
         The aspect applies the authentication rule and return FILTERED values
      */

  [NOT SO MOSTLY USED CONFIGS]
  2.5 - using @Secured
      - refer lesson 30
  2.6 - using @RolesAllowed
      - refer lesson 30

3. EntryPoint(s) :
   for web app its - Controller(Endpoints) - hence we can config authorization on endpoints
   for desktop app - we don't have spring security filter chain(filter chain is a concept only for webapps)
                   - for this we use concept called ASPECT(AOP), i.e we directly secure methods(generally of service layer)
                   - method ,level security we can apply for both WEB and DESKTOP apps

4. Using Above annotation to take method parameter
  - refer MethodLevelAdminService -> getByUsername()

5. In the example we discussed method level config on Service layer,
but it not only for Service, we can use these annotation anywhere
(i.e. on Controller, Service,Repo,proxy classes etc)
```

### Lecture 28( way 2 - Global method security : it uses AOP)

```lesson

1. use case for @PostAuthorize
- from our understanding we know where @PreAuthorize will be used.

- similarly use @PostAuthorize if we want to perform authorization rule
  on returned value (using returnObject) of the method
  - refer  -> testPostAuthorizeUseCase

2. In this lesson we have added examples for
 - @PreAuthorize
 - @PostAuthorize
 - @PreFilter
 - @PostFilter

 refer - MethodLevelAdminService
 refer - MethodLevelAdminController


```

### Lecture 29( way 2 - Global method security : it uses AOP)

```lesson
1. we can use combination of @PreAuthorize, @PostAuthorize, @PreFilter, @PostFilter
   on any method.

Ques - How can we use hasRole and hasAuthority on the same Endpoint or method ?
Ans - ?

2. Here we will study about hasPermission as @PreAuthorize(value = "hasPermission()")
refer - MethodLevelAdminService -> findMethodLevelAdminForUsername
/*
    hasPermission is an powerful tool, it handles complicated authorization rule
    in order to use hasPermission we need to create config class (refer -> MethodSecurityConfig)

    in MethodSecurityConfig we override createExpressionHandler method of GlobalMethodSecurityConfiguration
    there we attached a PermissionEvaluator object,

     PermissionEvaluator - this is an interface whose implementing class gives the place where
     we define authorization rule
     -  hasPermission(returnObject, 'ROLE_ADMIN') - calls
        PermissionEvaluator- hasPermission(Authentication authentication, Object targetDomainObject, Object permission)
     -  hasPermission(someObj1, someObj2, someObj3) - calls
        hasPermission(Authentication authentication, Serializable targetId, String targetType, Object permission);
     - refer MethodLevelAdminEvaluator


    Ques : How can we use Multiple PermissionEvaluator ?
    Ans : We can not, But we can use an alternative way to us hasPermission, tht will be discussed in lesson 30.
          which is more clean and recommended way of using hasPermission.
*/
```

### Lecture 30( way 2 - Global method security : it uses AOP)

```lesson
1. In the lesson 29 we studied configuring authorization rule using **hasPermission** using PermissionEvaluator
   - refer MethodLevelAdminEvaluator
   - refer MethodSecurityConfig
   - refer - MethodLevelAdminService -> findMethodLevelAdminForUsername

[RECOMMENDED OVER  PermissionEvaluator]
2. There is an alternative way of doing this
  - refer - MethodSecurityConfig -> methodLevelAdminAuthorizationManager()
  - refer - MethodLevelAdminAuthorizationManager
  - refer - MethodLevelAdminService -> findMethodLevelAdminForUsername2
  - Advantages
   1. Its more cleaner
   2. we can have any number of classes defining differ authorization rule as per our use case

3. @Secured and @RoleAllowed
  - in spring apps these annotation are rarely used.
  - spring sec team kept these annotation for compatibility with JAVA EE.
  - @Secured
    - refer - MethodLevelAdminService -> usingSecured()
  - @RoleAllowed
    - refer - MethodLevelAdminService -> usingRolesAllowed()
```