package com.replaceme.config;

import com.replaceme.dto.MethodLevelAdmin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.List;

/**
 * We have not annotated this class as @Component
 * this bean is created using @Bean in MethodSecurityConfig
 * refer - MethodSecurityConfig - methodLevelAdminAuthorizationManager()
 */
public class MethodLevelAdminAuthorizationManager {

    public Boolean applyHasPermission(List<MethodLevelAdmin> returnedList, String permission){

        System.out.println("MethodLevelAdminAuthorizationManager -> applyHasPermission");
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        System.out.println("MethodLevelAdminEvaluator -> applyHasPermission :: Authenticated username - "+username);
        System.out.println("MethodLevelAdminEvaluator -> applyHasPermission :: Permission required - "+permission);

        boolean docsBelongToTheAuthUser =returnedList.stream()
                .allMatch(d -> d.getUser().equals(username));

        boolean hasProperAuthority = authentication.getAuthorities().stream()
                .anyMatch(g -> g.getAuthority().equals(permission));

        return docsBelongToTheAuthUser && hasProperAuthority;
    }
}
