package com.replaceme.config;

import com.replaceme.authenticationprovider.MyUsernameOtpAuthenticationProvider;
import com.replaceme.authenticationprovider.MyUsernamePasswordAuthenticationProvider;
import com.replaceme.authenticationprovider.TokenAuthenticationProvider;
import com.replaceme.cache.TokenCache;
import com.replaceme.constant.MyConstants;
import com.replaceme.csrf.MyCustomCsrfTokenRepository;
import com.replaceme.entity.Otp;
import com.replaceme.filter.MyUsernamePasswordAuthFilter;
import com.replaceme.filter.TokenAuthFilter;
import com.replaceme.repo.OtpRepo;
import com.replaceme.service.MyUserDetailsManager;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;

import java.util.stream.Collectors;
import java.util.stream.Stream;

@EnableAsync // this is enabled to study SecurityContext in Multi-threaded env
@Configuration
public class ProjectConfig extends WebSecurityConfigurerAdapter {

    private MyUsernamePasswordAuthFilter myUsernamePasswordAuthFilter;

    private TokenAuthFilter tokenAuthFilter;

    @Autowired
    private OtpRepo otpRepo;

    @Autowired
    private TokenCache tokenCache;

    @Bean
    public UserDetailsManager userDetailsManager(){
        return new MyUserDetailsManager();
    }

    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Bean
    public MyUsernamePasswordAuthFilter myUsernamePasswordAuthFilterBean(AuthenticationManager authenticationManager) throws Exception {
        this.myUsernamePasswordAuthFilter = new MyUsernamePasswordAuthFilter(authenticationManager, otpRepo, tokenCache);
        return this.myUsernamePasswordAuthFilter;
    }

    @Bean
    public TokenAuthFilter tokenAuthFilterBean(AuthenticationManager authenticationManager) throws Exception {
        this.tokenAuthFilter = new TokenAuthFilter(authenticationManager);
        return this.tokenAuthFilter;
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // super.configure(http);

        /**
         * this is added so that spring DOES NOT cache
         * successful authentication
         */
        http.sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        // This is comment in lesson -23, as its OPTIONAL
        // we need to kee the below line if we want to use BasicAuthenticationFilter
        // else keep it commented
        // http.httpBasic();

        // Config csrf
        /*
        http.csrf(c -> {
                    c.ignoringAntMatchers("/csrfdisable/**");
                    c.csrfTokenRepository(new MyCustomCsrfTokenRepository());
                });
                // add a filter after csrf filter , in SS filter chain
                http.addFilterAfter(new MyCustomCsrfFilter(), CsrfFilter.class);
         */
        // disabling CSRF tokens
        http.csrf().disable();

        http.authorizeHttpRequests()
                .mvcMatchers("/users", "/understadingcors/**").permitAll()
                // .anyRequest().authenticated()
                ;

        /**
         * hasAuthority()
         * hasAnyAuthority()
         * hasRole()
         * hasAnyRole()
         * access() - // uses SpEL
         */
        /** ROLE VS AUTHORITY
         *
         * 1. ROLE meaning NOUN(e.g. manager, admin etc)
         *    AUTHORITY - meaning VERB(e.g. create,read,update, delete etc).
         *
         * 2. AUTHORITY mean more granular level permissions.
         * e.g. createUser
         *
         * 3. if we use ROLE we need to tell spring security to use GrantedAuthority
         * as ROLE by prefix "ROLE_"
         * EITHER before saving user to the db
         *   - refer - SpringbootAuthServerApplication -> run(String... args)
         * OR before adding authority to Authentication
         *   - refer - TokenAuthenticationProvider -> authenticate
         *
         * 4. ROLE means a group of permission
         *    e.g. For an ADMIN - createAdminAuthority, viewAdminAuthority, updateAdminAuthority etc.
         *    AUTHORITY means single permission
         *    e.g. createAdminAuthority - can only create an admin
         */
        http.authorizeHttpRequests()
                /**
                 * [MOST RECOMMENDED]
                 * mvc matcher
                 *  - it uses ANT path expression(s)
                 *  - spring recommends to use mvc matcher over other matcher
                 *
                 * ant matcher
                 *  - it also uses ANT path expression(s)
                 *  - spring recommends to use mvc matcher over ant matcher
                 *
                 * [LEAST RECOMMENDED]
                 * regex matchers
                 * - spring recommends to use mvc matcher over regex matcher
                 */
                /** ANT MATCHER SYNTAX */
                // 1. "/a/**" - any path which starts with "/a"  with ZERO or MULTIPLE
                //  - eg. of eligible path /a , /a/b, /a/b/c etc.

                // 2. "/a/*" -  - any path which starts with "/a" followed by EXACT ONE more /
                // - eg. of eligible path  /a/b etc
                // - /a , /a/b/c - ARE NOT A VALID PATH FOR THIS ANT MATCHER

                // 3. "/a/**/b" - any path which starts with "/a" followed by ZERO or MULTIPLE   and ends with /b
                // - eg. of eligible path /a/b , /a/b/b , /a/c/b, /a/b/c/a/b etc

                // 4. "/a/*/b" - any path which starts with "/a" followed by EXACT ONE and ends with /b
                // - eg. of eligible path  /a/b/b , /a/c/b etc
                // - /a/b , /a/b/c/a/b ARE NOT A VALID PATH FOR THIS ANT MATCHER
                .mvcMatchers("/manager/a/**").hasAnyRole(MyConstants.ROLE_MANAGERA)
                .mvcMatchers("/manager/b").hasAnyRole(MyConstants.ROLE_MANAGERB)
                .mvcMatchers("/manager/b/*").hasAnyRole(MyConstants.ROLE_MANAGERA,MyConstants.ROLE_MANAGERB)
                // adding authorization rule on @PathVariable
                // this is called Path Expression
                .mvcMatchers("/manager/c/{name2}").hasAnyRole(MyConstants.ROLE_MANAGERC)
                .mvcMatchers("/admin").hasRole(MyConstants.ROLE_ADMIN)
                /**
                 * authenticated() - means any authenticated user can access this endpoint
                 * regardless of its role
                 */
                .mvcMatchers("/any").authenticated()
                /**
                 * permitAll() - means any we are WHITELISTED this endpoint (GET /PATH )
                 * regardless of its role
                 */
                .mvcMatchers(HttpMethod.GET,"/public").permitAll()
                /**
                 * remaining all endpoints(PATH + HTTP VERB)
                 * */
                .anyRequest()
                /**
                 * AUTHORITY - meaning VERB
                 * if we use hasAuthority or hasAnyAuthority then
                 * at the time of creating Authentication object
                 * within AuthenticationProvider we can add authority as it is
                 * IN CASE OF hasRole OR hasAnyRole we need to prefix the string with ROLE_
                 * before saving it to Authentication within AuthenticationProvider
                 * refer - TokenAuthenticationProvider -> authenticate
                 */
                //.hasAnyAuthority("admin")
                /**
                 * ROLE - meaning NOUN
                 * if we use ROLE we need to tell spring security to use GrantedAuthority
                 * as ROLE by prefix "ROLE_"
                 * EITHER before saving user to the db
                 *   - refer - SpringbootAuthServerApplication -> run(String... args)
                 * OR before adding authority to Authentication
                 *   - refer - TokenAuthenticationProvider -> authenticate
                 */
                //.hasAnyRole(MyConstants.ROLE_ADMIN)
                // from lesson 27- start
                /*
                once authenticated, Method level config will take control
                of authorization
                if we set permitAll() then Authentication object will be null
                 */
                // from lesson 27- end
                .authenticated()
        ;

        http.addFilterBefore(myUsernamePasswordAuthFilter, BasicAuthenticationFilter.class);
        http.addFilterBefore(tokenAuthFilter,  BasicAuthenticationFilter.class);

        http.cors(c -> {
            CorsConfigurationSource ccs = r -> {
                CorsConfiguration cc = new CorsConfiguration();
                cc.setAllowedOrigins(Stream.of("*").collect(Collectors.toList()));
                cc.setAllowedMethods(
                        Stream.of(
                                HttpMethod.GET.toString(),
                                HttpMethod.POST.toString())
                                .collect(Collectors.toList()));
                return cc;
            };

            c.configurationSource(ccs);
        });
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth
            .authenticationProvider(new MyUsernamePasswordAuthenticationProvider(userDetailsManager(), passwordEncoder()))
            .authenticationProvider(new MyUsernameOtpAuthenticationProvider(userDetailsManager(), otpRepo))
            .authenticationProvider(new TokenAuthenticationProvider(tokenCache, userDetailsManager()))
        ;
    }

    @Bean
    public InitializingBean initializingBean(){
        return ()->{
            SecurityContextHolder.setStrategyName(SecurityContextHolder.MODE_INHERITABLETHREADLOCAL);
        };
    }
}
