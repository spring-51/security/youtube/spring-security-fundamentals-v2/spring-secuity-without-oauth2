package com.replaceme.config;

import com.replaceme.dto.MethodLevelAdmin;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;

import java.io.Serializable;
import java.util.List;

public class MethodLevelAdminEvaluator implements PermissionEvaluator {
    @Override
    public boolean hasPermission(Authentication authentication,
                                 Object targetObject,
                                 Object permission) {
        System.out.println("MethodLevelAdminEvaluator -> hasPermission-1");
        List<MethodLevelAdmin> returnedList = (List<MethodLevelAdmin>) targetObject;
        String username = authentication.getName();
        System.out.println("MethodLevelAdminEvaluator -> hasPermission-1 :: Authenticated username - "+username);
        String auth = (String) permission;
        System.out.println("MethodLevelAdminEvaluator -> hasPermission-1 :: Permission required - "+permission);

        boolean docsBelongToTheAuthUser =returnedList.stream()
                .allMatch(d -> d.getUser().equals(username));

        boolean hasProperAuthority = authentication.getAuthorities().stream()
                .anyMatch(g -> g.getAuthority().equals(auth));

        return docsBelongToTheAuthUser && hasProperAuthority;
    }

    @Override
    public boolean hasPermission(
            Authentication authentication,
            Serializable targetId,
            String type,
            Object permission) {
        return false;
    }
}
