package com.replaceme.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.access.expression.method.DefaultMethodSecurityExpressionHandler;
import org.springframework.security.access.expression.method.MethodSecurityExpressionHandler;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.method.configuration.GlobalMethodSecurityConfiguration;

/**
 * This config class is added in lesson 29
 * it's used for hasPermission SpEL in Authorization config annotation
 * refer MethodLevelAdminService -> findMethodLevelAdminForUsername
 */
// method level(added in lesson 27 in ProjectConfig and moved to here in lesson 29) - start
@EnableGlobalMethodSecurity(
        prePostEnabled = true // for enabling @PreAuthorize, @PostAuthorize, @PreFilter, @PostFilter

        // lesson 30 - start

        ,securedEnabled = true // for enabling @Secured
        ,jsr250Enabled = true  // for enabling @RolesAllowed

        // lesson 30 - end
)
/**
 * @PreAuthorize  --> prePostEnabled = true
 * @PostAuthorize --> prePostEnabled = true
 * @PreFilter  --> prePostEnabled = true
 * @PreFilter  --> prePostEnabled = true
 *
 * @Secured  --> securedEnabled = true
 * @RolesAllowed  --> jsr250Enabled = true
 *
 * in order to understand more about the above annotations refer README -> Lesson 27
 * MethodLevelAdminService -> getAll()
 */
// method level(added in lesson 27 in ProjectConfig and moved to here in lesson 29) - start
@Configuration
public class MethodSecurityConfig extends GlobalMethodSecurityConfiguration {

    // injected in lesson 30
    @Autowired
    private ApplicationContext applicationContext;

    // lesson 29 - start

    @Override
    protected MethodSecurityExpressionHandler createExpressionHandler() {
        DefaultMethodSecurityExpressionHandler handler = new DefaultMethodSecurityExpressionHandler();

        handler.setPermissionEvaluator(permissionEvaluator());
        // lesson 30 - start
        /*
        The below line added to enable MethodLevelAdminAuthorizationManager bean to
        be used in place of hasPermission
         */
        handler.setApplicationContext(applicationContext);
        // lesson 30 - end
        return handler;
    }

    private PermissionEvaluator permissionEvaluator(){
        return new MethodLevelAdminEvaluator();
    }

    // lesson 29 - end

    // lesson 30 - start

    @Bean(name = "methodLevelAdminAuthorizationManager")
    public MethodLevelAdminAuthorizationManager methodLevelAdminAuthorizationManager(){
        return  new MethodLevelAdminAuthorizationManager();
    }

    // lesson 30 - end
}
