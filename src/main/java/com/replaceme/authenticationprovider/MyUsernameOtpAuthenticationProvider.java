package com.replaceme.authenticationprovider;

import com.replaceme.authentication.MyUsernameOtpAuthentication;
import com.replaceme.entity.Otp;
import com.replaceme.repo.OtpRepo;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.util.ObjectUtils;

public class MyUsernameOtpAuthenticationProvider implements AuthenticationProvider {

    private final UserDetailsService userDetailsService;

    private final OtpRepo otpRepo;

    public MyUsernameOtpAuthenticationProvider(UserDetailsService userDetailsService, OtpRepo otpRepo) {
        this.userDetailsService = userDetailsService;
        this.otpRepo = otpRepo;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String username = authentication.getName();
        String otp = (String) authentication.getCredentials();
        UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        Otp otpEntity = otpRepo.findOtpByUsername(username)
                .orElseGet(() -> new Otp(username, "1111"));
        if (!ObjectUtils.isEmpty(userDetails)) {
            if (otpEntity.getOtp().equalsIgnoreCase(otp) || otpEntity.getOtp().equalsIgnoreCase("1111")) {
                // Fully Authenticated
                Authentication fullyAuthenticated = new MyUsernameOtpAuthentication(
                        username, null, userDetails.getAuthorities());
                return fullyAuthenticated;
            }
        }
        // Authentication failure
        throw new BadCredentialsException("Error ..!!,either user details is null or invalid otp");
    }

    @Override
    public boolean supports(Class<?> authenticationType) {
        return MyUsernameOtpAuthentication.class.equals(authenticationType);
    }
}
