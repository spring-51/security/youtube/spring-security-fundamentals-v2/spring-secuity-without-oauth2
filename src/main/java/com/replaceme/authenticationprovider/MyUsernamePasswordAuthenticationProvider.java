package com.replaceme.authenticationprovider;

import com.replaceme.authentication.MyUsernamePasswordAuthentication;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.util.ObjectUtils;

public class MyUsernamePasswordAuthenticationProvider implements AuthenticationProvider {

    private final UserDetailsService userDetailsService;
    
    private final PasswordEncoder passwordEncoder;

    public MyUsernamePasswordAuthenticationProvider(UserDetailsService userDetailsService, PasswordEncoder passwordEncoder) {
        this.userDetailsService = userDetailsService;
        this.passwordEncoder = passwordEncoder;
    }

    /**
     *
     * return Fully Authentication object, if this AP passes request authentication - DONE
     * throws Exception, if this AP failed request authentication - DONE
     * return null if this AP can't decide and passes authentication responsibility to other AP - PENDING
     *
     * @param authentication
     * @return
     * @throws AuthenticationException
     */
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String username = authentication.getName();
        String password = (String) authentication.getCredentials();
        UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        if (!ObjectUtils.isEmpty(userDetails)) {
            if (passwordEncoder.matches(password, userDetails.getPassword())) {
                // Fully Authenticated
                Authentication fullyAuthenticated = new MyUsernamePasswordAuthentication(
                        username, null, userDetails.getAuthorities());
                return fullyAuthenticated;
            }
        }
        // Authentication failure
        throw new BadCredentialsException("Error ..!!,either user details is null or invalid creds");
    }

    @Override
    public boolean supports(Class<?> authenticationType) {
        return MyUsernamePasswordAuthentication.class.equals(authenticationType);
    }
}
