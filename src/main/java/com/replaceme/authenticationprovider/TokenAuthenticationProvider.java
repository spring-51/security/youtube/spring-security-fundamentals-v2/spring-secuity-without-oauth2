package com.replaceme.authenticationprovider;

import com.replaceme.authentication.TokenAuthentication;
import com.replaceme.cache.TokenCache;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.provisioning.UserDetailsManager;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class TokenAuthenticationProvider implements AuthenticationProvider {

    private final TokenCache tokenCache;

    private final UserDetailsManager userDetailsManager;

    public TokenAuthenticationProvider(TokenCache tokenCache, UserDetailsManager userDetailsManager) {
        this.tokenCache = tokenCache;
        this.userDetailsManager = userDetailsManager;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String token = authentication.getName();
        if(tokenCache.contains(token)){
            String username =  token.split("-")[0];

            /**
             IN CASE OF .hasAuthority OR .hasAnyAuthority we  DO NOT prepend ROLE_  to the string
             * config in app security config class
             */
            // authorities.add(new SimpleGrantedAuthority(role));
            /**
             * IN CASE OF .hasRole OR .hasAnyRole we prepend ROLE_  to the string
             * config in app security config class
             * ROLE_ will tell that GrantedAuthority in Authentication object is
             * a ROLE
             */
            // authorities.add(new SimpleGrantedAuthority("ROLE_"+role));
            UserDetails userDetails = userDetailsManager.loadUserByUsername(username);
            Collection authorities = userDetails.getAuthorities();
            return new TokenAuthentication(token, null, authorities);
        }
        throw new BadCredentialsException("Invalid tokennn :(");
    }

    @Override
    public boolean supports(Class<?> authenticationType) {
        return TokenAuthentication.class.equals(authenticationType);
    }
}
