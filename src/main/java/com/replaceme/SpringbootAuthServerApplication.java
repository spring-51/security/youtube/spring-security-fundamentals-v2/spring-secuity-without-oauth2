package com.replaceme;

import com.replaceme.constant.MyConstants;
import com.replaceme.entity.MyUser;
import com.replaceme.repo.MyUserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication
public class SpringbootAuthServerApplication implements CommandLineRunner {
	@Autowired
	private MyUserRepo repo;

	@Autowired
	private PasswordEncoder passwordEncoder;

	public static void main(String[] args) {
		SpringApplication.run(SpringbootAuthServerApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		/**AUTHORITY*/
		/*
		repo.save(
				new MyUser(
						"admin",
						passwordEncoder.encode("admin-h2"),
						"admin"));
		repo.save(
				new MyUser(
						"manager",
						passwordEncoder.encode("manager-h2"),
						"manager"));
		 */

		/**ROLE*/
		repo.save(
				new MyUser(
						"admin",
						passwordEncoder.encode("admin-h2"),
						"ROLE_"+ MyConstants.ROLE_ADMIN));
		repo.save(
				new MyUser(
						"managera",
						passwordEncoder.encode("manager-h2"),
						"ROLE_"+MyConstants.ROLE_MANAGERA));

		repo.save(
				new MyUser(
						"managerb",
						passwordEncoder.encode("manager-h2"),
						"ROLE_"+MyConstants.ROLE_MANAGERB));

		repo.save(
				new MyUser(
						"managerc",
						passwordEncoder.encode("manager-h2"),
						"ROLE_"+MyConstants.ROLE_MANAGERC));

	}
}
