package com.replaceme.service;

import com.replaceme.dto.MethodLevelAdmin;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PreFilter;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import javax.annotation.security.RolesAllowed;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class MethodLevelAdminService {

    // lesson 27 - start
    /*
    @PreAuthorize(value = "hasRole('ADMIN')")
    the authorization rules are validated against the current Authentication Object
    before calling the below method and decide whether to call the below method or return 403
    without calling the method.

    internally the aspect(AOP) does the above validation and as per validation result
    it either calls the method or reject it with 403
     */
    //@PreAuthorize(value = "hasRole('ADMIN')")

    /*
    @PostAuthorize(value = "hasRole('ADMIN')")
    the method is called ALWAYS and only then aspect validates the authorization rules against the current Authentication Object
    and decide whether to return response from the method or return 403.

    internally the aspect(AOP) does the above validation AFTER CALLING the method and method executed
    and as per validation result it either return the response from the method or return 403

    use case - use @PostAuthorize if we want to perform authorization
    rule on returned value (using returnObject) of the method
    - refer MethodLevelAdminService -> testPostAuthorizeUseCase
     */
    @PostAuthorize(value = "hasRole('ADMIN')")
    /*
    @PreFilter(value = ?)
    The method needs to have THE PARAMETER value of Collection or Array else IT WILL NOT WORK
    The aspect intercept the method call and validates the value inside the the collection or
    array parameter
     */
    /*
     @PostFilter(value = ?)
      The method needs to have THE RETURN value of Collection or Array else IT WILL NOT WORK.
      The aspect applies the authentication rule and return FILTERED values
     */
    public List<String> getAll(Authentication authentication){
        return Stream
                .of("method-level-admin-1","method-level-admin-2")
                .collect(Collectors.toList());
    }

    /*
    PreAuthorize authorizing rule based on parameter value
     #<parameter-name>  == <some-value>
     */
    @PreAuthorize("#usernamee == authentication.name")
    public List<String> getByUsername(String usernamee){
        return Stream
                .of("getByUsername-method-level-admin-1","getByUsername-method-level-admin-2")
                .collect(Collectors.toList());
    }
    // lesson 27 - end

    // lesson 28 - start
    // lesson 28 - Pre/Post Authorize - start

    /*
    @PreAuthorize(value = "hasRole('ADMIN')")
    the authorization rules are validated against the current Authentication Object
    before calling the below method and decide whether to call the below method or return 403
    without calling the method.

    internally the aspect(AOP) does the above validation and as per validation result
    it either calls the method or reject it with 403
     */
    @PreAuthorize(value = "hasRole('ADMIN')")
    public List<String> testPreAuthorize() {
        System.out.println("testPreAuthorize -> this will be printed ONLY IF AUTHORIZATION SUCCEEDED");
        return Stream
                .of("testPreAuthorize-method-level-admin-1","testPreAuthorize-method-level-admin-2")
                .collect(Collectors.toList());
    }

    /*
    @PostAuthorize(value = "hasRole('ADMIN')")
    the method is called ALWAYS and only then aspect validates the authorization rules against the current Authentication Object
    and decide whether to return response from the method or return 403.

    internally the aspect(AOP) does the above validation AFTER CALLING the method and method executed
    and as per validation result it either return the response from the method or return 403

    use case - use @PostAuthorize if we want to perform authorization
    rule on returned value (using returnObject) of the method
    - refer MethodLevelAdminService -> testPostAuthorizeUseCase

     */
    @PostAuthorize(value = "hasRole('ADMIN')")
    public List<String> testPostAuthorize() {
        System.out.println("testPostAuthorize -> this will ALWAYS be printed, BUT THE RESPONSE WILL ONLY BE RETURNED IF AUHORIZATION RULE SUCCEEDED");
        return Stream
                .of("testPostAuthorize-method-level-admin-1","testPostAuthorize-method-level-admin-2")
                .collect(Collectors.toList());
    }

    /*
    use case - use @PostAuthorize if we want to perform authorization
    rule on returned value (using returnObject) of the method
     */
    @PostAuthorize(value = "returnObject == authentication.name")
    public String testPostAuthorizeUseCase(Boolean flag, Authentication authentication) {
        System.out.println("testPostAuthorizeUseCase -> this will ALWAYS be printed, BUT THE RESPONSE WILL ONLY BE RETURNED IF AUHORIZATION RULE SUCCEEDED");
        if(flag){
            return authentication.getName();
        }else{
            return "invalid-name";
        }
    }

    // lesson 28 - Pre/Post Authorize - End

    // lesson 28 - Pre/Post Filter - start

    /*
    filterObject  - in case of @PreFilter is all the values that passes as method parameter
     */
    @PreFilter(value = "filterObject == authentication.name")
    public List<String> testPreFilter(List<String> list){
        /*
        as per above rule "filterObject == 'john'"
        it means whatever element we passed in the list
        only "john" will stay in the list when this method started execution
         */
        System.out.println("testPreFilter -> list "+list);
        return list;
    }

    /*
    filterObject  - in case of @PostFilter is all the values that returned from method
     */
    @PostFilter(value = "filterObject != authentication.name")
    public List<String> testPostFilter(List<String> list){
        /*
        as per above rule "filterObject == 'john'"
        it means whatever element we passed in the list
        only "john" wil stay in the list when this method's ASPECT END execution
         */
        System.out.println("testPostFilter -> list "+list);
        return list;
    }
    // lesson 28 - Pre/Post Filter - End

    // lesson 28 - end

    // lesson 29 - start
    /*
    hasPermission is an powerful tool, it handles complicated authorization rule
    in order to use hasPermission we need to create config class (refer -> MethodSecurityConfig)

    in MethodSecurityConfig we override createExpressionHandler method of GlobalMethodSecurityConfiguration
    there we attached a PermissionEvaluator object,

     PermissionEvaluator - this is an interface whose implementing class gives the place where
     we define authorization rule
     -  hasPermission(returnObject, 'ROLE_ADMIN') - calls
        PermissionEvaluator- hasPermission(Authentication authentication, Object targetDomainObject, Object permission)
     -  hasPermission(someObj1, someObj2, someObj3) - calls
        hasPermission(Authentication authentication, Serializable targetId, String targetType, Object permission);
     - refer MethodLevelAdminEvaluator

     Ques : How can we use Multiple PermissionEvaluator ?
     Ans : ?

     NOTE:
     ALTERNATIVE WAY OF DOING hasPermission is  shown in findMethodLevelAdminForUsername2
     */
    @PostAuthorize("hasPermission(returnObject, 'ROLE_ADMIN')")
    public List<MethodLevelAdmin> findMethodLevelAdminForUsername(String username){
        System.out.println("MethodLevelAdminService -> findMethodLevelAdminForUsername");
        MethodLevelAdmin methodLevelAdmin = new MethodLevelAdmin();
        methodLevelAdmin.setUser(username);
        return Stream.of(methodLevelAdmin).collect(Collectors.toList());
    }

    // lesson 29 - end

    // lesson 30 - start

    /*
    This way of using hasPermission is recommended over PermissionEvaluator, Because
     1. The code is cleaner as compared to PermissionEvaluator, since we don't need casting here
     2. We can config multiple authorization config classes or method as per our use case
        where as in case of  PermissionEvaluator we can create ONLY ONE class, which has 2 methods ONLY
     */
    /*
    in order to use bean within PostAuthorize we need to tell spring.
    we tell spring by passing applicationContext to DefaultMethodSecurityExpressionHandler
    refer - MethodSecurityConfig -> createExpressionHandler()
     */
    @PostAuthorize("@methodLevelAdminAuthorizationManager.applyHasPermission(returnObject, 'ROLE_ADMIN')")
    public List<MethodLevelAdmin> findMethodLevelAdminForUsername2(String username){
        System.out.println("MethodLevelAdminService -> findMethodLevelAdminForUsername");
        MethodLevelAdmin methodLevelAdmin = new MethodLevelAdmin();
        methodLevelAdmin.setUser(username);
        return Stream.of(methodLevelAdmin).collect(Collectors.toList());
    }

    /*
     @Secured - we can only pass allowed roles, its not as powerful as Pre/Post annotation
     since they use SpEL, where as @Secured does not use SpEL

    it's considered as specific case of @PreAuthorize
     - we can considered @Secured as  @PreAuthorize which validates only Role
     */
    @Secured(value = {"ROLE_ADMIN"})
    public List<MethodLevelAdmin> usingSecured(){
        System.out.println("MethodLevelAdminService -> findMethodLevelAdminUsingSecured");
        MethodLevelAdmin methodLevelAdmin = new MethodLevelAdmin();
        methodLevelAdmin.setUser("test-user-usingSecured");
        return Stream.of(methodLevelAdmin).collect(Collectors.toList());
    }

    /*
     @RolesAllowed - we can only pass allowed roles, its not as powerful as Pre/Post annotation
     since they use SpEL, where as @RolesAllowed does not use SpEL.

     it's considered as specific case of @PreAuthorize
     - we can considered @RolesAllowed as  @PreAuthorize which validates only Role
     */
    @RolesAllowed(value = {"ROLE_ADMIN"})
    public List<MethodLevelAdmin> usingRolesAllowed(){
        System.out.println("MethodLevelAdminService -> findMethodLevelAdminUsingSecured");
        MethodLevelAdmin methodLevelAdmin = new MethodLevelAdmin();
        methodLevelAdmin.setUser("test-user-usingRolesAllowed");
        return Stream.of(methodLevelAdmin).collect(Collectors.toList());
    }

    // lesson 30 - end
}
