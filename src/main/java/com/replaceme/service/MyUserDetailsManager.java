package com.replaceme.service;

import com.replaceme.dto.MyUserDetails;
import com.replaceme.entity.MyUser;
import com.replaceme.repo.MyUserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;

public class MyUserDetailsManager implements UserDetailsManager {

    @Autowired
    private MyUserRepo userRepo;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        // getting user from db
        MyUser myUser = userRepo.findUserByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("Errorrrr-> user not found"));

        // transforming it to UserDetails and then returning
        return new MyUserDetails(myUser);
    }

    @Override
    public void createUser(UserDetails user) {
        userRepo.save(new MyUser(user.getUsername(), passwordEncoder.encode(user.getPassword()), "admin"));
    }

    @Override
    public void updateUser(UserDetails user) {

    }

    @Override
    public void deleteUser(String username) {

    }

    @Override
    public void changePassword(String oldPassword, String newPassword) {

    }

    @Override
    public boolean userExists(String username) {
        return false;
    }
}
