package com.replaceme.repo;

import com.replaceme.entity.MyUser;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MyUserRepo extends CrudRepository<MyUser, Long> {
    Optional<MyUser> findUserByUsername(String username);
}
