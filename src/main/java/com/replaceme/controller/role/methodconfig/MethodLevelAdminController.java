package com.replaceme.controller.role.methodconfig;

import com.replaceme.dto.MethodLevelAdmin;
import com.replaceme.service.MethodLevelAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequestMapping("/method/admin")
public class MethodLevelAdminController {

    private String className;
    {
        className = this.getClass().getTypeName();
    }
    @Autowired
    private MethodLevelAdminService methodLevelAdminService;

    // lesson 27 - start
    @GetMapping
    public List<String> getAll(Authentication authentication){
        String operation = String.format("%s -> %s :", className, "getAll");
        System.out.println(operation+"-"+authentication);
        return methodLevelAdminService.getAll(authentication);
    }

    @GetMapping("/{user}")
    public List<String> getByUsername(@PathVariable String user, Authentication authentication){
        String operation = String.format("%s -> %s :", className, "getByUsername");
        System.out.println(operation+ "-user-" + user);
        System.out.println(operation+ "-authentication-" + authentication.getName());
        return methodLevelAdminService.getByUsername(user);
    }
    // lesson 27 - end

    // lesson 28 - start
    @GetMapping("/pre-authorize")
    public List<String> testPreAuthorize(Authentication authentication){
        String operation = String.format("%s -> %s :", className, "testPreAuthorize");
        System.out.println(operation);
        return methodLevelAdminService.testPreAuthorize();
    }

    @GetMapping("/post-authorize")
    public List<String> testPostAuthorize(Authentication authentication){
        String operation = String.format("%s -> %s :", className, "testPostAuthorize");
        System.out.println(operation);
        return methodLevelAdminService.testPostAuthorize();
    }

    @GetMapping("/post-authorize/usecase")
    public String testPostAuthorize(@RequestParam(value = "f") Boolean flag, Authentication authentication){
        String operation = String.format("%s -> %s :", className, "testPostAuthorize");
        System.out.println(operation);
        return methodLevelAdminService.testPostAuthorizeUseCase(flag, authentication);
    }

    @GetMapping("/pre-filter")
    public List<String> testPreFilter(Authentication authentication){
        String operation = String.format("%s -> %s :", className, "testPreFilter");
        System.out.println(operation);
        List<String> list = Stream.of(authentication.getName(),"testPreFilter-1", "testPreFilter-2")
                .collect(Collectors.toList());
        System.out.println(operation +" - request list - "+list);
        List<String> response = methodLevelAdminService.testPreFilter(list);
        System.out.println(operation +" - response list - "+response);
        return response;
    }

    @GetMapping("/post-filter")
    public List<String> testPostFilter(Authentication authentication){
        String operation = String.format("%s -> %s :", className, "testPostFilter");
        System.out.println(operation);
        List<String> list = Stream.of(authentication.getName(),"testPostFilter-1", "testPostFilter-2")
                .collect(Collectors.toList());
        System.out.println(operation +" - request list - "+list);
        List<String> response = methodLevelAdminService.testPostFilter(list);
        System.out.println(operation +" - response list - "+response);
        return response;
    }
    // lesson 28 - end
    // lesson 29 - start

    @GetMapping(value = "/by-username/{user}")
    public List<MethodLevelAdmin> findMethodLevelAdminForUsername(
            @PathVariable(value = "user") String username){
        return methodLevelAdminService.findMethodLevelAdminForUsername(username);
    }

    // lesson 29 - end

    // lesson 30 - start

    @GetMapping(value = "/by-username2/{user}")
    public List<MethodLevelAdmin> findMethodLevelAdminForUsername2(
            @PathVariable(value = "user") String username){
        return methodLevelAdminService.findMethodLevelAdminForUsername2(username);
    }

    @GetMapping(value = "/using-secured")
    public List<MethodLevelAdmin> usingSecured(){
        return methodLevelAdminService.usingSecured();
    }

    @GetMapping(value = "/using-roles-allowed")
    public List<MethodLevelAdmin> usingRolesAllowed(){
        return methodLevelAdminService.usingRolesAllowed();
    }

    // lesson 30 - end
}
