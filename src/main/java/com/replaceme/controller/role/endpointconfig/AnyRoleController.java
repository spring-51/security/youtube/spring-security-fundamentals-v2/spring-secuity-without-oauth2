package com.replaceme.controller.role.endpointconfig;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/any")
public class AnyRoleController {

    @GetMapping
    public  String testGet(){
        return "Hello From AnyRoleController -> testGet" ;
    }
}
