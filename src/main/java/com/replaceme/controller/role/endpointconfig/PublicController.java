package com.replaceme.controller.role.endpointconfig;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/public")
public class PublicController {

    @GetMapping
    public  String testGet(){
        return "Hello From PublicController -> testGet" ;
    }

    @PostMapping
    public  String testPost(){
        return "Hello From PublicController -> testPost" ;
    }
}
