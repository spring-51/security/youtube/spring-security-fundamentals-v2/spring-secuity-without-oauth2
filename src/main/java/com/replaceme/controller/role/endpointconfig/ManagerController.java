package com.replaceme.controller.role.endpointconfig;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/manager")
public class ManagerController {

    @GetMapping(value = "/a")
    public  String testGetA(){
        return "Hello From ManagerController -> testGetA -> A" ;
    }

    @GetMapping(value = "/b")
    public  String testGetB(){
        return "Hello From ManagerController -> testGetB -> B" ;
    }

    @GetMapping(value = "/a/b")
    public  String testGetAB(){
        return "Hello From ManagerController -> testGetAB -> A B" ;
    }

    @GetMapping(value = "/b/c")
    public  String testGetBC(){
        return "Hello From ManagerController -> testGetBC -> B C" ;
    }

    @GetMapping(value = "/a/b/c")
    public  String testGetABC(){
        return "Hello From ManagerController -> testGetABC -> A B C" ;
    }

    @GetMapping(value = "/c/{name}")
    public  String testGetC(@PathVariable String name){
        return String.format("Hello From ManagerController -> testGetC -> %s", name)  ;
    }

}
