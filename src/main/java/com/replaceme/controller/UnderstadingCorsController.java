package com.replaceme.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(value = "/understadingcors")
public class UnderstadingCorsController {

    @GetMapping
    public String get(){
        return "main.html";
    }

    @PostMapping("/test")
    @ResponseBody
    // NOT A GOOD PRACTICE TO PLACE @CrossOrigin AT METHOD LEVEL
    // GOOD PRACTICE IS TO USE  CORS cofig. **GLOBALLY** () refer ProjectConfig-> configure(HttpSecurity)
    // @CrossOrigin(origins = {"*"})
    public String test() {
        System.out.println(":(");
        return "TEST!";
    }
}
