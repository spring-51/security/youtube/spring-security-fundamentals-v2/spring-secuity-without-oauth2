package com.replaceme.controller;

import org.springframework.scheduling.annotation.Async;
import org.springframework.security.concurrent.DelegatingSecurityContextExecutor;
import org.springframework.security.concurrent.DelegatingSecurityContextRunnable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/tokens")
public class TokenController {

    @GetMapping(value = "/1")
    public String getToken1(Authentication authentication){
        return Thread.currentThread().getName()+" - jwt.token.response, authentication -> "+authentication.getName();
    }

    @GetMapping(value = "2")
    public String getToken2(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return Thread.currentThread().getName()+" - jwt.token.response, authentication -> "+authentication.getName();
    }

    @GetMapping(value = "async")
    @Async
    public String getTokenAsync(){

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        System.out.println(Thread.currentThread().getName()+", "+authentication);
        return Thread.currentThread().getName()+" - jwt.token.response, authentication -> "+authentication.getName();

    }
}
