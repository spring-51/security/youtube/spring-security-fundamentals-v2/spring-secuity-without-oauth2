package com.replaceme.controller;

import com.replaceme.dto.MyUserDetails;
import com.replaceme.entity.MyUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/users")
public class UserController {

    @Autowired
    private UserDetailsManager userDetailsManager;

    @PostMapping
    public MyUser create(@RequestBody MyUser request){
        MyUserDetails userDetails = new MyUserDetails(request);
        userDetailsManager.createUser(userDetails);
        return request;
    }
}
