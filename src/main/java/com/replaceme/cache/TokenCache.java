package com.replaceme.cache;

import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class TokenCache {

    private Set<String> tokens = new HashSet<>();

    public void add(String token){
        tokens.add(token);
    }

    public Boolean contains(String token){
        return this.tokens.contains(token);
    }
}
