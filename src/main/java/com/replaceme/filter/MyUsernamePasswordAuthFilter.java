package com.replaceme.filter;

import com.replaceme.authentication.MyUsernameOtpAuthentication;
import com.replaceme.authentication.MyUsernamePasswordAuthentication;
import com.replaceme.cache.TokenCache;
import com.replaceme.entity.Otp;
import com.replaceme.repo.OtpRepo;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Random;
import java.util.UUID;

/**
 * this filter will be used when we want to validate
 * username and password.
 *
 */
@Component
public class MyUsernamePasswordAuthFilter extends OncePerRequestFilter {


    private final AuthenticationManager authenticationManager;

    private final OtpRepo otpRepo;

    private final TokenCache tokenCache;

    public MyUsernamePasswordAuthFilter(
            AuthenticationManager authenticationManager,
            OtpRepo otpRepo,
            TokenCache tokenCache) {
        this.authenticationManager = authenticationManager;
        this.otpRepo = otpRepo;
        this.tokenCache = tokenCache;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

        // Step 1: Username and password
        // Step 2 : username and otp
        String otp =  request.getHeader("otp");
        String username =  request.getHeader("username");
        if(!ObjectUtils.isEmpty(otp)){
            // Step 1: Username and password
            // authenticate using otp
            MyUsernameOtpAuthentication authentication
                    = new MyUsernameOtpAuthentication(username,otp);
            Authentication fullyAuthenticate = authenticationManager.authenticate(authentication);
            if(fullyAuthenticate.isAuthenticated()){
                // SecurityContextHolder.getContext().setAuthentication(fullyAuthenticate);
                String token = UUID.randomUUID().toString();
                String tokenWithUsername = String.format("%s-%s", username, token);
                response.setHeader("token", tokenWithUsername);
                tokenCache.add(tokenWithUsername);
            }
        }else{
            // Step 2 : username and otp
            // authenticate using username and password
            String password =  request.getHeader("password");
            MyUsernamePasswordAuthentication authentication
                    = new MyUsernamePasswordAuthentication(username,password);
            Authentication fullyAuthenticate = authenticationManager.authenticate(authentication);
            if(fullyAuthenticate.isAuthenticated()){
                String otpToSave = String.valueOf(new Random().nextInt(9999)+1000);
                Otp savedOtp = otpRepo.save(new Otp(username, otpToSave));
                response.setHeader("otp", savedOtp.getOtp());
                // SecurityContextHolder.getContext().setAuthentication(fullyAuthenticate);
            }
        }
    }

    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
        return !request.getServletPath().equals("/login");
    }
}
