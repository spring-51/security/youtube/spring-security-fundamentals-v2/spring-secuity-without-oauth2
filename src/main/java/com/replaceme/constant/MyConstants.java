package com.replaceme.constant;

public class MyConstants {
    public static final String ROLE_ADMIN = "ADMIN";
    public static final String ROLE_MANAGERA = "MANAGERA";
    public static final String ROLE_MANAGERB = "MANAGERB";
    public static final String ROLE_MANAGERC = "MANAGERC";
}
