package com.replaceme.dto;

import com.replaceme.entity.MyUser;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MyUserDetails implements UserDetails {

    private final MyUser userFromDb;

    public MyUserDetails(MyUser userFromDb) {
        this.userFromDb = userFromDb;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Stream.of(userFromDb.getAuthority())
                .map(e -> new SimpleGrantedAuthority(e))
                .collect(Collectors.toList());
    }

    @Override
    public String getPassword() {
        return userFromDb.getPassword();
    }

    @Override
    public String getUsername() {
        return userFromDb.getPassword();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
